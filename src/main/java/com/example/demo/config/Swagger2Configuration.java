package com.example.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author User
 * @date 2020/8/12 21:25
 */
@Configuration
@EnableSwagger2
public class Swagger2Configuration {
    @Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.example.demo.controller"))
                .paths(PathSelectors.any())
                .build();
    }

//    private ApiInfo apiInfo() {
//        return new ApiInfoBuilder()
//                .title("单表嵌套查询练习")
//                .description("单表嵌套查询练习在线文档")
////                .termsOfServiceUrl("/")
//                .version("1.0")
//                .build();
//    }

    private ApiInfo apiInfo() {
        // springfox.documentation.service.Contact
        Contact contact = new Contact("团队名", "http://localhost:9999/swagger-ui.html", "my@my.com");
        return new ApiInfoBuilder()
                .title("单表嵌套查询练习")
                .description("单表嵌套查询练习在线文档")
                .contact(contact)   // 联系方式
                .version("1.0.0")  // 版本
                .build();
    }
}
