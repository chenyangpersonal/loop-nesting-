package com.example.demo.controller;

import com.example.demo.entity.Classs;
import com.example.demo.entity.School;
import com.example.demo.service.SchoolService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.annotation.Resource;
import javax.xml.crypto.Data;
import java.util.Date;
import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: SchoolController
 * @date 2020/10/18 18:48
 */
@RestController
@Api(tags = "查询")
public class SchoolController {

    @Resource
    private SchoolService schoolService;


    @ApiOperation("根据创建班级日期查询所有学校以及班级，学生")
    @GetMapping("/findalltime")
    @ApiImplicitParam(name = "createTime",value = "创建班级时间",dataType = "Date")
    public List<School> findalltime(@ApiIgnore Classs classs){
        return this.schoolService.findalltime(classs);
    }

    /**
    * @Description: findtime
    * @Param: 
    * @return: * @return: java.util.List<com.example.demo.entity.School>
    * @Author: 陈阳
    * @Date: 2020/10/20 
    * @Time: 16:33
    */
    @ApiOperation("查询所有学校，班级，学生")
    @GetMapping("/findall")
    public List<School> findtime(){
        return this.schoolService.findalltime();
    }

    @ApiOperation("用xml查询所有学校，班级，学生")
    @GetMapping("/find")
    public List<School> find(){
        return this.schoolService.findallall();
    }

    @ApiOperation("批量添加学校")
    @PostMapping("/addall")
    public void addall(@RequestBody List<School> schools){
        this.schoolService.addall(schools);
    }

    @ApiOperation("批量修改学校")
    @PostMapping("/updateall")
    public void updateall(@RequestBody List<School> schools){
        this.schoolService.updateall(schools);
    }

    @ApiOperation("批量删除学校")
    @PostMapping("/delall")
    public void delall(@RequestBody List<School> schools){
        this.schoolService.delall(schools);
        System.err.println();
        System.err.println();
        System.err.println();System.err.println();
        System.err.println();
        System.err.println();
        System.err.println();System.err.println();
        System.err.println();
        System.err.println();
        System.err.println();System.err.println();
        System.err.println();
        System.err.println();
        System.err.println();System.err.println();

    }

}
