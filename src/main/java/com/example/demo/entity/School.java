package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 学校
 * @date 2020/10/18 18:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_school")
public class School extends BaseEntity implements Serializable {

    /**
     * 学校名称
     */
    private String name;

    /**
     * 学校地址
     */
    private String address;

    /**
     * 班级列表
     */
    @TableField(exist = false)
    private List<Classs> classsList;
}
