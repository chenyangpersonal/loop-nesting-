package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 学生
 * @date 2020/10/18 18:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_student")
public class Student extends BaseEntity implements Serializable {

    /**
     * 学生姓名
     */
    private String name;

    /**
     * 学生年龄
     */
    private Integer age;

    /**
     * 学生性别
     */
    private Integer sex;

    /**
     * 学生地址
     */
    private String address;

    /**
     * 班级id
     */
    private Integer classsId;
}
