package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 班级
 * @date 2020/10/18 18:18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_classs")
public class Classs extends BaseEntity implements Serializable {

    /**
     * 班级名称
     */
    private String name;

    /**
     * 学校编号
     */
    private Integer schoolId;

    /**
     * 学生列表
     */
    @TableField(exist = false)
    private List<Student> studentList;


}
