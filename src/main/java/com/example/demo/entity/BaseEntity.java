package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: 公共实体类
 * @date 2020/10/18 18:08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel
public class BaseEntity {
    /*
    自增量
     */
    @TableId(value = "id",type = IdType.AUTO)
    @ApiModelProperty("id自增")
    private Integer id;
    /*
    唯一标识
     */
    @ApiModelProperty("唯一标识")
    private String code;
    /*
    删除标志   0表示  正常   1表示  删除
     */
    @ApiModelProperty("删除标志   0表示  正常   1表示  删除")
    private Integer del;

    /*
    这条记录的创建时间
     */
    @ApiModelProperty("创建时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createTime;

    /*
    这条记录的最后修改时间
     */
    @ApiModelProperty("修改时间")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updateTime;
}
