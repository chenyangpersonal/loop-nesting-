package com.example.demo.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Classs;
import com.example.demo.mapper.ClasssMapper;
import com.example.demo.service.ClasssService;
import org.springframework.stereotype.Service;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: ClasssServiceImpl
 * @date 2020/10/18 18:38
 */
@Service
public class ClasssServiceImpl extends ServiceImpl<ClasssMapper, Classs> implements ClasssService {
}
