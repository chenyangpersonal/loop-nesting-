package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Classs;
import com.example.demo.entity.School;
import com.example.demo.entity.Student;
import com.example.demo.mapper.SchoolMapper;
import com.example.demo.service.ClasssService;
import com.example.demo.service.SchoolService;
import com.example.demo.service.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: SchoolServiceImpl
 * @date 2020/10/18 18:41
 */
@Service
public class SchoolServiceImpl extends ServiceImpl<SchoolMapper, School> implements SchoolService {


    @Resource
    private SchoolMapper schoolMapper;

    @Resource
    private StudentService studentService;

    @Resource
    private ClasssService classsService;

    /***
     * 根据条件查询学校
    * @Description: findalltime
    * @Param: * @param schoolss:
    * @return: * @return: java.util.List<com.example.demo.entity.School>
    * @Author: 陈阳
    * @Date: 2020/10/19
    * @Time: 14:09
    */
    @Override
    public List<School> findalltime(Classs schoolss) {
        QueryWrapper<Classs> classsQueryWrapper = new QueryWrapper<>();
        classsQueryWrapper.lambda().gt(Classs::getCreateTime,schoolss.getCreateTime());
        //根据日期查询班级
        List<Classs> list = classsService.list(classsQueryWrapper);
        List<School> schools = new ArrayList<>();

        for (Classs a:list){
            //根据班级查询所对应的学校
            School school = this.schoolMapper.selectById(a.getSchoolId());

            QueryWrapper<Classs> classsQueryWrapper1 = new QueryWrapper<>();
            classsQueryWrapper1.lambda().eq(Classs::getSchoolId, school.getId());
            //根据学校查询所在的班级
            List<Classs> list1 = this.classsService.list(classsQueryWrapper1);
            //带走
            school.setClasssList(list1);
            //根据班级查询所在的学生
            for(Classs b: list1){

                QueryWrapper<Student> studentQueryWrapper = new QueryWrapper<>();
                studentQueryWrapper.lambda().eq(Student::getClasssId,b.getId());
                List<Student> list2 = studentService.list(studentQueryWrapper);
                //带走
                b.setStudentList(list2);
            }
            schools.add(school);
        }
        return schools;
    }


    @Override
    public List<School> findalltime() {

        return this.list();
    }

    @Override
    public List<School> findallall() {
        return this.schoolMapper.findallall();
    }

    @Override
    public void addall(List<School> schools) {
        this.saveBatch(schools);
    }

    @Override
    public void updateall(List<School> schools) {
        this.updateBatchById(schools);
    }

    @Override
    public void delall(List<School> schools) {
        this.removeByIds(schools);
    }

    @Override
    public void aaaa() {
        String a = "hjbqjhdbhjbjhbjhbhj";
    }

}
