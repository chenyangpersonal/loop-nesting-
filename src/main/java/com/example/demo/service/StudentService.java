package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Student;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: studentService
 * @date 2020/10/18 18:37
 */
public interface StudentService extends IService<Student> {
}
