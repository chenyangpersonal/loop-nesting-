package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Classs;
import com.example.demo.entity.School;

import java.util.Date;
import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: Schoolservice
 * @date 2020/10/18 18:35
 */
public interface SchoolService extends IService<School> {

    /**
     * 查询一定时间的学校。班级。学生
    * @Description: findalltime
    * @Param: * @param school:
    * @return: * @return: java.util.List<com.example.demo.entity.School>
    * @Author: 陈阳
    * @Date: 2020/10/19
    * @Time: 14:49
    */
    public List<School> findalltime(Classs school);

    /**
     * 查询所有学校。班级。学生
    * @Description: findalltime
    * @Param:
    * @return: * @return: java.util.List<com.example.demo.entity.School>
    * @Author: 陈阳
    * @Date: 2020/10/19
    * @Time: 14:49
    */
    public List<School> findalltime();

    /**
     * 查询所有学校
    * @Description: findallall
    * @Param:
    * @return: * @return: java.util.List<com.example.demo.entity.School>
    * @Author: 陈阳
    * @Date: 2020/10/19
    * @Time: 14:49
    */
    public List<School> findallall();

    /**
     * 批量添加学校
    * @Description: addall
    * @Param: * @param schools:
    * @return: * @return: void
    * @Author: 陈阳
    * @Date: 2020/10/19
    * @Time: 14:49
    */
    public void addall(List<School> schools);

    /***
     *
     * 批量修改学校
    * @Description: updateall
    * @Param: * @param schools:
    * @return: * @return: void
    * @Author: 陈阳
    * @Date: 2020/10/18
    * @Time: 20:54
    */
    public void updateall(List<School> schools);

    /**
     * 批量删除学校
    * @Description: delall
    * @Param: * @param schools:
    * @return: * @return: void
    * @Author: 陈阳
    * @Date: 2020/10/18
    * @Time: 20:57
    */
    public void delall(List<School> schools);

    /***
     *
    * @Description: aaaa
    * @Param: 
    * @return: * @return: void
    * @Author: 陈阳
    * @Date: 2020/10/19 
    * @Time: 14:25
    */
    public void aaaa();
}
