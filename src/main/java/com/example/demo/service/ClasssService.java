package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Classs;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: ClasssService
 * @date 2020/10/18 18:37
 */
public interface ClasssService extends IService<Classs> {
}
