package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Classs;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: ClassMapper
 * @date 2020/10/18 18:33
 */
@Mapper
public interface ClasssMapper extends BaseMapper<Classs> {
}
