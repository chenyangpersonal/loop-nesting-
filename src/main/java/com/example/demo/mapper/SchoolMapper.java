package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.School;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 陈阳
 * @progrom: IntelliJ IDEA
 * @description: StudentMapper
 * @date 2020/10/18 18:32
 */
@Mapper
public interface SchoolMapper extends BaseMapper<School> {

    public List<School> findallall();
}
